---
# Display name
title: Benjamin Wolfe

# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: Data Scientist

# Organizations/Affiliations to show in About widget
organizations:
- name: Munich Re Digital Partners
  url: https://www.munichre.com/digital-partners/

# Short bio (displayed in user profile at end of posts)
bio: Benjamin loves data visualization, R programming, music, tea, the great outdoors, math, writing, mechanical keyboards, and backyard astronomy. Tweets <a href="https://twitter.com/BenjaminWolfe">@BenjaminWolfe</a>.

# Interests to show in About widget
interests:
- analytics community building
- data visualization
- package development
- reproducibility + sustainability

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/BenjaminWolfe
  label: Follow me on Twitter
- icon: github
  icon_pack: fab
  link: https://github.com/BenjaminWolfe
- icon: envelope
  icon_pack: fas
  link: 'mailto:benjamin.e.wolfe@gmail.com'
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/benjaminwolfe/
- icon: cv
  icon_pack: ai
  link: https://benjamin-wolfe.netlify.app/files/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "benjamin.e.wolfe@gmail.com"

# Highlight the author in author lists? (true/false)
highlight_name: true

user_groups:
- Meet the Team

authors:
- benjamin
---

Benjamin is a data scientist at [@DP_InsurTech][digital-partners],
recreational [#rstats][rstats] developer, 
lifelong learner, analytics community builder, and tidyverse fan.

I live in Austin, TX with my wife Karin and kids Brylie & Evan,
all of us Bay Area transplants. 
When I'm not at my laptop, you can find me with my white lab, Ivy,
my [Roo hammock][roo], my binoculars, and my poetry journal.

{{< icon name="download" pack="fas" >}} Download my (somewhat outdated) {{< staticref "https://benjamin-wolfe.netlify.app/files/resume.pdf" "newtab" >}}resumé{{< /staticref >}}.

[digital-partners]: https://www.twitter.com/DP_InsurTech
[rstats]: https://www.twitter.com/#rstats
[roo]: https://kammok.com/products/roo-double-camping-hammock
