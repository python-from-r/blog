---
# Display name
title: Laurie Baker

# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: Data Science Lecturer

# Organizations/Affiliations to show in About widget
organizations:
- name: Data Science Campus, Office for National Statistics, United Kindom
  url: https://datasciencecampus.ons.gov.uk/

# Short bio (displayed in user profile at end of posts)
bio: Laurie is a data science lecturer at the Data Science Campus, Office for National Statistics. She is a disease ecologist and marine biologist by training with a keen interesting in programming and data science. She loves the ocean, knitting, learning languages (Italian, Spanish, Portuguese), and anything outdoors.Tweets <a href="https://twitter.com/llbaker1707">@llbaker1707</a>.

# Interests to show in About widget
interests:
- open science
- spatial statistics
- disease ecology
- marine biology

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/llbaker1707
  label: Follow me on Twitter
- icon: github
  icon_pack: fab
  link: https://github.com/LaurieLBaker
- icon: envelope
  icon_pack: fas
  link: 'mailto:laurie.baker@glasgow.ac.uk'
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/dr-laurie-baker-893a6338
- icon: cv
  icon_pack: ai
  link: https://lauriebaker/rbind.io

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "laurie.baker@glasgow.ac.uk"

# Highlight the author in author lists? (true/false)
highlight_name: true

user_groups:
- Meet the Team

authors:
- laurie
---

Laurie is a data science lecturer at the [Data Science Campus](https://datasciencecampus.ons.gov.uk/), Office for National Statistics and an affiliate researcher at the University of Glasgow. She is a disease ecologist and marine biologist by training with a keen interest in programming, spatial statistics, and data science.

Originally from Maine, I live in Glasgow, Scotland. On the weekends you can find me cycling, camping, learning to fish, or hiking in beautiful Scotland.

Find out more about my research and teaching on my [personal website](https://lauriebaker/rbind.io).

