# REMEMBER to restart R after you modify and save this file!

# First, execute the global .Rprofile if it exists. You may configure blogdown
# options there, too, so they apply to any blogdown projects. Feel free to
# ignore this part if it sounds too complicated to you.
if (file.exists("~/.Rprofile")) {
  base::sys.source("~/.Rprofile", envir = environment())
}

# packages required to build the blog; see .gitlab-ci.yml!
required_packages <- c(
  "blogdown",
  "here",
  "janitor",
  "showtext",
  "ggrepel",
  "scales",
  "gt",
  "glue"
)

# for more blogdown options, see
# https://bookdown.org/yihui/blogdown/global-options.html
blog_options <- list(
  # to automatically serve the site on RStudio startup, set this option to TRUE
  blogdown.serve_site.startup = FALSE,
  # to disable knitting Rmd files on save, set this option to FALSE
  blogdown.knit.on_save = TRUE,
  # build .Rmd to .html (via Pandoc); to build to Markdown, set this option to 'markdown'
  blogdown.method = 'html',
  # fix Hugo version
  blogdown.hugo.version = "0.80.0"
)

local({
  # check for packages; if they're not there, throw an informative error
  installed <- required_packages |>
    sapply(\(x) require(x, character.only = T)) |>
    suppressWarnings() |>
    suppressMessages()
  if (any(!installed)) {
    required_packages[!installed] |>
      shQuote() |>
      paste(collapse = ", ") |>
      (\(x) paste0("please use install.packages(c(", x, "))"))() |>
      stop()
  }

  # silently check for the Hugo version we're using
  # if it's not there `find_hugo()` will throw an informative error
  invisible(blogdown::find_hugo(blog_options$blogdown.hugo.version))
})

set_options <- function(blog_options) {
  setNames <- stats::setNames
  capture.output <- utils::capture.output
  subset_true <- function(x) {
    # assumes x is a named logical vector
    # returns TRUE values
    x[x]
  }

  self_name <- function(x) {
    # name something after itself
    setNames(x, x)
  }

  # are any our options already set (to something other than NA)?
  already_set <-
    blog_options |>
    names() |>
    self_name() |>
    sapply(\(x) !is.na(getOption(x, default = NA)))

  if (any(already_set)) {
    # if any are already set, are any of those different from what we want?
    mismatched <-
      already_set |>
      subset_true() |>
      names() |>
      self_name() |>
      sapply((\(x) getOption(x) != blog_options[[x]]))

    if (any(mismatched)) {
      # get desired values. quote them if strings. raise an informative error.
      mismatched_names <-
        mismatched |>
        subset_true() |>
        names()

      opts_list <-
        capture.output(dput(blog_options[mismatched_names])) |>
        trimws() |>
        paste(collapse = " ")
      stop("please use options(", opts_list, ")")
    }
  }

  if (any(!already_set)) {
    # now we can set options courteously
    # knowing that any that are already set already match
    opts_list <-
      capture.output(dput(blog_options[!already_set])) |>
      trimws() |>
      paste(collapse = " ")
    message("setting the following, previously either unset or NA: ", opts_list)
    options(blog_options[!already_set])
  }
}

set_options(blog_options)

rm(list = c("required_packages", "blog_options", "set_options"))
