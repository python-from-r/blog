window.moveTOC = function() {
  var tocElements = $("#TOC").clone().children();
  var toc = $('<nav id="TableOfContents">')
    .addClass("nav flex-column")
    .append(tocElements);
  $("div.docs-toc").append(toc);
}
